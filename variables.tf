# AWS
variable "aws_region" {
  description = "The AWS region to host the resources"
  default = "us-west-2"
}

variable "aws_availability_zone" {
  type = string
  description = "The AWS availability zone within the specified region to host the resources"
}

variable "aws_zone_id" {
  type = string
  description = "The AWS Route53 zone ID"
}

# Networking
variable "aws_vpc_id" {
  type = string
  description = "The VPC to host the resources"
}

variable "aws_subnet_id" {
  type = string
  description = "The VPC subnet ID to host the resources"
}

# Security
variable "security_group_name" {
  type = string
  description = "The VPC security group name for traffic to and from the instance"
}

variable "role_name" {
  type = string
  description = "The IAM role name for accessing the bucket"
}

variable "instance_profile_name" {
  type = string
  description = "The EC2 instance profile name which assumes the IAM role for accessing the bucket"
}

variable "users_json" {
  type = string
  description = "The JSON data containing user credentials"
}

# DNS
variable "subdomain" {
  type = string
  description = "The subdomain name under the main domain"
}

# Storage
variable "bucket" {
  type = string
  description = "The S3 bucket to store packages and load configuration"
}

# Packages
variable "packages_scope" {
  type = string
  description = "The scope of packages the registry can host"
  default = "**"
}

variable "packages_access_role" {
  type = string
  description = "The role a user must have to access a package"
  default = "authenticated"
}

variable "packages_publish_role" {
  type = string
  description = "The role a user must have to publish a package"
  default = "authenticated"
}

variable "packages_unpublish_role" {
  type = string
  description = "The role a user must have to un-publish a package"
  default = "authenticated"
}

# Tagging
variable "instance_name" {
  type = string
  description = "The EC2 instance Name tag"
}

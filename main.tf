locals {
  availability_zone = "${var.aws_region}${var.aws_availability_zone}"
}

data "aws_ami" "main_ami" {
  owners = ["self"]
  most_recent = true

  filter {
    name = "name"
    values = ["npm-packages-*"]
  }
}

resource "aws_s3_bucket" "main_bucket" {
  bucket = var.bucket
  acl = "private"
  versioning {
    enabled = false
  }
}

resource "aws_s3_bucket_object" "users_object" {
  bucket = aws_s3_bucket.main_bucket.bucket
  key = "users.json"
  content = var.users_json
}

resource "aws_iam_role" "main_role" {
  name = var.role_name
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "main_instance_profile" {
  name = var.instance_profile_name
  role = var.role_name
}

resource "aws_iam_role_policy" "main_role_policy" {
  name = "main"
  role = aws_iam_role.main_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ["s3:ListBucket"],
      "Resource": ["arn:aws:s3:::${var.bucket}"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": ["arn:aws:s3:::${var.bucket}/*"]
    }
  ]
}
EOF
}

resource "aws_security_group" "main_security_group" {
  name = var.security_group_name
  vpc_id = var.aws_vpc_id

  # SSH
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # All outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "main_instance" {
  ami = data.aws_ami.main_ami.id
  availability_zone = local.availability_zone
  instance_type = "t2.micro"
  subnet_id = var.aws_subnet_id
  vpc_security_group_ids = [
    aws_security_group.main_security_group.id
  ]
  associate_public_ip_address = true
  iam_instance_profile = aws_iam_instance_profile.main_instance_profile.id
  user_data = <<EOF
    PACKAGES_BUCKET=${var.bucket}
    PACKAGES_SCOPE=${var.packages_scope}
    PACKAGES_ACCESS_ROLE=${var.packages_access_role}
    PACKAGES_PUBLISH_ROLE=${var.packages_publish_role}
    PACKAGES_UNPUBLISH_ROLE=${var.packages_unpublish_role}
  EOF
  tags = {
    Name = var.instance_name
  }
  depends_on = [
    aws_s3_bucket_object.users_object
  ]
}

resource "aws_route53_record" "main_record" {
  zone_id = var.aws_zone_id
  name = var.subdomain
  type = "A"
  ttl = "300"
  records = [
    aws_instance.main_instance.public_ip
  ]
}
#!/bin/bash
. /var/lib/cloud/instance/user-data.txt
aws s3 cp s3://$PACKAGES_BUCKET/users.json /home/terraform
aws s3 cp s3://$PACKAGES_BUCKET/tokens.json /home/terraform
verdaccio-utilities add-users \
  --config="$1" \
  --file=/home/terraform/users.json \
  --merge-out=/home/terraform/tokens.json \
  --allow-conflict \
  --force
aws s3 cp /home/terraform/tokens.json s3://$PACKAGES_BUCKET/tokens.json

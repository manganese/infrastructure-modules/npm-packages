variable "aws_region" {
  type = string
  default = "us-west-2"
}

locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

source "amazon-ebs" "npm-packages" {
  ami_name = "npm-packages-${local.timestamp}"
  instance_type = "t2.micro"
  region = var.aws_region

  source_ami_filter {
    filters = {
      name = "ubuntu/images/hvm-ssd/ubuntu-hirsute-21.04-amd64-server-*"
      root-device-type = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners = ["099720109477"] # Canonical
  }

  ssh_username = "ubuntu"
}

build {
  sources = ["source.amazon-ebs.npm-packages"]

  provisioner "file" {
    source = "./verdaccio-config.yml"
    destination = "/tmp/verdaccio-config.yml"
  }

  provisioner "file" {
    source = "./verdaccio-before-start.sh"
    destination = "/tmp/verdaccio-before-start.sh"
  }

  provisioner "shell" {
    script = "./setup.sh"
  }
}
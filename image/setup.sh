#!/bin/bash
set -x

sudo apt-get update -y
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade
sudo apt-get update
sudo apt-get -y -qq install curl jq

# Setup sudo to allow no-password sudo for "hashicorp" group and adding "terraform" user
sudo groupadd -r hashicorp
sudo useradd -m -s /bin/bash terraform
sudo usermod -a -G hashicorp terraform
sudo cp /etc/sudoers /etc/sudoers.orig
echo "terraform  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/terraform

# Install AWS CLI
sudo apt-get install -y awscli

# Install Node.js
sudo curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install Verdaccio
sudo npm install -g \
  verdaccio \
  verdaccio-aws-s3-storage \
  verdaccio-utilities \
  substitute-with

# Copy Verdaccio config
sudo cp /tmp/verdaccio-config.yml /home/terraform

# Copy before start script
sudo cp /tmp/verdaccio-before-start.sh /home/terraform

# Link Verdaccio service
sudo verdaccio-utilities link-service \
  --config=/home/terraform/verdaccio-config.yml \
  --before-script=/home/terraform/verdaccio-before-start.sh \
  --aws
sudo systemctl daemon-reload
sudo systemctl enable verdaccio

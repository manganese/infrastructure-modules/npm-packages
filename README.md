# Infrastructure Module - NPM Packages

This reusable infrastructure module supports a NPM-compatible package registry.  Under the hood, it uses [Verdaccio](https://verdaccio.org).
